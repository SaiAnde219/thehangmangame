package data;

/**
 * Created by sai on 9/22/16.
 */
import java.util.List;
import java.util.Set;

public class GameDataJSONGenerator {
    public String getTargetWord() {
        return targetWord;
    }

    public void setTargetWord(String targetWord) {
        this.targetWord = targetWord;
    }

    public String targetWord;

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    public void setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
    }

    public Set<Character> badGuesses;

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    public void setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
    }

    public Set<Character> goodGuesses;

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void setRemainingGuesses(int remainingGuesses) {
        this.remainingGuesses = remainingGuesses;
    }

    public int remainingGuesses;


}
